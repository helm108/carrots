extends Area2D

signal player_focus_gained
signal player_focus_lost
signal player_interacted_with

var is_focused: bool = false

func _on_InteractableArea_area_entered(_area):
	emit_signal("player_focus_gained")
	is_focused = true

func _on_InteractableArea_area_exited(_area):
	emit_signal("player_focus_lost")
	is_focused = false
	
func _input(_event):
	if Input.is_action_just_pressed("interact") and is_focused:
		emit_signal("player_interacted_with")
