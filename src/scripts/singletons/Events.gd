# warning-ignore-all:unused_signal

extends Node

# Game flow.
signal day_end(message)
signal day_process(message)
signal day_start(message)
signal player_move(message)
signal allow_input(message)
signal introduction_ended(message)

# Item selection.
signal select_tool(message)

# Farming.
signal focused_tile(message)

# Inventory.
signal seed_count_update(message)
signal money_count_update(message)

# NPC interaction.
signal dialogic_signal(message)
signal npc_interacted_with(message)
