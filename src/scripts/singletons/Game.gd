extends Node

enum Tools { HANDS, SHOVEL, WATERING_CAN, SEEDS }

var day: = 1
var currentScene: = 'SampleScene'
var currentWorld: = 'CozyFarm'
var currentTool: int = Tools.HANDS

var npcs = {}
var plants: = {}

var reputation_max = 5
var seed_value: = 1
var carrot_value: = 5
var seeds: = 0
var money: = 0
var money_goal: = 480

func _ready():
	Events.connect('day_process', self, '_day_end')
	Events.connect('select_tool', self, '_select_tool')
	
func _day_end():
	day += 1
	Events.emit_signal('day_start')
	
func _select_tool(selected_tool):
	currentTool = selected_tool

func get_plant_name(pos: Vector2):
	return "{x}-{y}".format({"x": pos.x, "y": pos.y})
	
func get_plant(pos: Vector2):
	var plant_name = get_plant_name(pos)
	if Game.plants.has(plant_name):
		return Game.plants[plant_name]
	return false

func delete_plant(plant_name: String):
	Game.plants[plant_name].scene.queue_free()
	Game.plants.erase(plant_name)

func add_money(amount: int):
	money += amount
	Events.emit_signal("money_count_update", money)

func add_seeds(amount: int):
	seeds += amount
	Events.emit_signal("seed_count_update", seeds)
