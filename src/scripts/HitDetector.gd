extends Area2D

onready var world: Node2D = get_node('/root/' + Game.currentScene + '/' + Game.currentWorld)
var ground: TileMap
var prev_tile_pos: Vector2 = Vector2()

func _ready():
	ground = world.get_node('Farmable')

func _process(_delta):
	_update_focused_tile()

func _update_focused_tile():
	var tile_pos = ground.world_to_map(ground.to_local(global_position))
	if tile_pos != prev_tile_pos:
		Events.emit_signal("focused_tile", tile_pos)
		prev_tile_pos = tile_pos
