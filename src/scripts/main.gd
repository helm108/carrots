extends Node2D

func _ready():
	Events.connect("day_start", self, "_handle_day_start")
	
	# This could be in conversation manager's ready function, but this felt better.
	# A scene's ready function is not called until its children have called ready.
	$ConversationManager.call_deferred('play_scene', 'introduction')

func _handle_day_start():
	# If the player has hit the target goal they've won the game.
	if Game.money >= Game.money_goal:
		$ConversationManager.call_deferred('play_scene', 'game_won')
	
	# If they have no plants, no seeds and not enough money to buy seeds then they have lost the game.
	if Game.money <= Game.seed_value && Game.seeds <= 0 && Game.plants.size() <= 0:
		$ConversationManager.call_deferred('play_scene', 'game_lost')
