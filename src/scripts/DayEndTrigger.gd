extends Area2D

func _on_DayEndTrigger_body_entered(body):
	if body.name == 'Player':
		Events.emit_signal('day_end')
