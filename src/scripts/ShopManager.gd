extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	Events.connect("dialogic_signal", self, "_dialogic_handler")

var seed_signals: = ['buy_seed_1', 'buy_seed_5', 'buy_seed_10']

func _dialogic_handler(message):
	if seed_signals.has(message):
		var components = message.split('_')
		_buy_seeds(int(components[2]))
		
func _buy_seeds(count: int):
	var cost = count * Game.seed_value
	if cost > Game.money:
		return
	
	Game.add_money(-cost)
	Game.add_seeds(count)
