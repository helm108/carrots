extends HBoxContainer

var selected_color: = Color(0, 0, 0, 1)
var deselected_color: = Color(0, 0, 0, 0)

func _ready():
	var _error = Events.connect("select_tool", self, "_select_tool")
	_select_tool(Game.currentTool)
	
func _select_tool(selected_tool):
	$ShovelBackground.self_modulate = deselected_color
	$WateringCanBackground.self_modulate = deselected_color
	$HandsBackground.self_modulate = deselected_color
	$SeedsBackground.self_modulate = deselected_color
	
	match selected_tool:
		Game.Tools.SHOVEL:
			$ShovelBackground.self_modulate = selected_color
		Game.Tools.WATERING_CAN:
			$WateringCanBackground.self_modulate = selected_color
		Game.Tools.HANDS:
			$HandsBackground.self_modulate = selected_color
		Game.Tools.SEEDS:
			$SeedsBackground.self_modulate = selected_color
