extends Node2D

func _ready():
	Events.connect("day_start", self, '_move_player')

func _move_player():
	Events.emit_signal("player_move", global_position)
