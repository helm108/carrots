extends Node2D

var trigger_has_focus = false

func _on_InteractableArea_player_interacted_with():
	if trigger_has_focus:
		Events.emit_signal("day_end")
		
func _on_InteractableArea_player_focus_gained():
	trigger_has_focus = true

func _on_InteractableArea_player_focus_lost():
	trigger_has_focus = false
