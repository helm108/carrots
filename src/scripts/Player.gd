extends KinematicBody2D

var speed: int = 150
var velocity: = Vector2()
var direction: = 'east'
var target_body: Node2D = null
var allow_input: = true;

func _ready():
	z_index = int(position.y)
	Events.connect("player_move", self, '_set_position')
	Events.connect("allow_input", self, '_handle_allow_input')

func _handle_allow_input(_allow_input):
	allow_input = _allow_input;

func _physics_process(_delta):
	if !allow_input:
		return
	
	z_index = int(position.y)
	
	velocity = Vector2()
	if Input.is_action_pressed('walk_right'):
		velocity.x += 1
	if Input.is_action_pressed('walk_left'):
		velocity.x -= 1
	if Input.is_action_pressed('walk_down'):
		velocity.y += 1
	if Input.is_action_pressed('walk_up'):
		velocity.y -= 1
	
	velocity = velocity.normalized() * speed
	velocity = move_and_slide(velocity)

func _process(_delta):
	if !allow_input:
		$AnimatedSprite.play('stand_%s' % direction)
		return
		
	if Input.is_action_pressed('walk_right'):
		direction = 'east'
		$AnimatedSprite.play('walk_east')
		$HitDetector.position = Vector2(32, 16)
	elif Input.is_action_pressed('walk_left'):
		direction = 'west'
		$AnimatedSprite.play('walk_west')
		$HitDetector.position = Vector2(-32, 16)
	elif Input.is_action_pressed('walk_down'):
		direction = 'south'
		$AnimatedSprite.play('walk_south')
		$HitDetector.position = Vector2(0, 48)
	elif Input.is_action_pressed('walk_up'):
		direction = 'north'
		$AnimatedSprite.play('walk_north')
		$HitDetector.position = Vector2(0, -16)
	else:
		$AnimatedSprite.play('stand_%s' % direction)
		
func _set_position(position: Vector2):
	global_position = position
	direction = 'south'
