extends Node2D

onready var root: Node = get_node('/root')
var scene_playing: bool = false
var default_conversation_template = "%s-day%s"

# Called when the node enters the scene tree for the first time.
func _ready():
	var _error: = Events.connect('npc_interacted_with', self, '_npc_interaction_handler')
	DialogicSingleton.init(true)

func timeline_exists(timeline_name) -> bool:
	var timelines = DialogicUtil.get_timeline_list()
	for timeline in timelines:
		if timeline.name == timeline_name:
			return true
	
	return false

var timelines_played = []

func play_scene(scene_name):
	if scene_playing: return
	if !timeline_exists(scene_name): return
	
	Dialogic.set_variable("scene_played", int(timelines_played.has(scene_name)))
	
	var scene = Dialogic.start(scene_name, false)
	
	scene_playing = true
	scene.connect('timeline_end', self, '_dialogic_timeline_end')
	scene.connect("dialogic_signal", self, "_dialogic_handler")
	
	timelines_played.append(scene_name)
	
	Events.emit_signal('allow_input', false)
	
	root.add_child(scene)
	
func _dialogic_handler(message):
	Events.emit_signal("dialogic_signal", message)

func play_conversation(npc_definition, day):
	# Default pattern is `name-day#`, e.g. `janet-day2`.
	var scene_name = default_conversation_template % [npc_definition.id, day]
	
	# Override scene under specific conditions.
	match npc_definition.id:
		'janet':
			match day:
				1:
					scene_name = 'janet-welcome'
		'steven':
			match day:
				1:
					scene_name = 'steven-welcome'
		'finlight':
			Dialogic.set_variable('money', Game.money)
			scene_name = 'buy-seeds'

	play_scene(scene_name)

func _npc_interaction_handler(npc_definition):
	play_conversation(npc_definition, Game.day)

func _dialogic_timeline_end(_timeline_name):
	# _timeline_name is the filename and not the name you give it. Really unhelpful.
	if _timeline_name == 'timeline-1635611784.json':
		Events.emit_signal("introduction_ended")
	
	Events.emit_signal('allow_input', true)
	scene_playing = false
	_check_reputation()

func _check_reputation():
	for id in Game.npcs:
		var npc = Game.npcs[id]
		var rep = Dialogic.get_variable(npc.id + "-rep")
		if rep == '' || rep == null:
			push_error("Rep for " + npc.id + " is empty.")
			
		
		npc.reputation = rep
