extends Sprite

var default_modulate = Color(0, 0, 0, 0.2)
var focus_modulate = Color(1, 0, 0, 1)

func _on_InteractableArea_player_focus_gained():
	modulate = focus_modulate

func _on_InteractableArea_player_focus_lost():
	modulate = default_modulate
