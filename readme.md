# The Archive

A game made in Godot.

# Milestones
- [x] Basic
  - [x] Have a character walk around a map
  - [x] Collide with water
  - [x] Collide with edge of map
- [x] Dialogic Demo
  - [x] Have two NPCs standing on the map
  - [x] Interact with NPCs to trigger conversations
- [x] Day Cycle MVP
  - [x] Interact with a bed to end the day
  - [x] Have different conversations with NPCs
- [x] Farming
  - [x] Selectable tools
  - [x] Hoe grass to create soil tile
    - [x] Detect tile under HitDetector
    - [x] Highlight tile under HitDetector
    - [x] Only highlight if Shovel is selected
    - [x] Convert to soil when E pressed
  - [x] Tile is highlighted if it can be interacted with
  - [x] Plant seeds on soil
  - [x] Water soil with Watering Can
  - [x] Plants grow as time passes
  - [x] Plants die if time passes and soil is not watered
  - [x] Plants can be harvested and converted to money
- [ ] Story Features
  - [x] Day Cycle
    - [x] Fade to black before processing everything
    - [x] Fade back in once next day has been set up 
  - [x] If a day ends with no seeds and no money, end the game
  - [x] Win condition - trigger conversation when money goal hit
  - [x] Tutorial
  - [x] Introduction
    - [x] Obtaining watering can and shovel
  - [x] End of Day Gate
    - [x] Walkable tile that ends the day
    - [x] Spawn point for player at day start
  - [x] Buy seeds
  - [x] Create NPCs
    - [x] Parents
    - [x] Friends
    - [x] Dialogic portraits for NPCs and PC
  - [ ] Make dad appear again for win and loss conversations
- [ ] Rabbits (Nice to Have)
  - [ ] Rabbits eating carrots randomly
  - [x] NPC reputation system
    - [x] Store reputation value against each NPC
    - [x] Conversations that increase or decrease rep
  - [ ] NPC rabbit defence
    - [ ] NPCs become defenders once a reputation threshold is reached
    - [ ] NPCs stationed around farm

# Attribution
- Farm tools https://vayasandesu.itch.io/farm-tool-icon-24x24
- Modern interior characters https://limezu.itch.io/moderninteriors
- World tiles https://shubibubi.itch.io/cozy-farm
- Key icons https://thoseawesomeguys.com/prompts/
- Additional icons and map design https://twitter.com/pinktrashcat
- Built with Godot https://godotengine.org
